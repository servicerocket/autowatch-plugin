/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.autowatch;

import com.atlassian.confluence.event.events.content.attachment.AttachmentCreateEvent;
import com.atlassian.confluence.event.events.content.attachment.AttachmentEvent;
import com.atlassian.confluence.event.events.content.attachment.AttachmentUpdateEvent;
import com.atlassian.confluence.event.events.content.blogpost.BlogPostCreateEvent;
import com.atlassian.confluence.event.events.content.blogpost.BlogPostEvent;
import com.atlassian.confluence.event.events.content.blogpost.BlogPostUpdateEvent;
import com.atlassian.confluence.event.events.content.comment.CommentCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.event.Event;
import com.atlassian.event.EventListener;
import com.atlassian.user.User;

/**
 * Listens for comments and adds the listener as a watcher of the page/news
 * item.
 */
public class AutowatchListener implements EventListener {
    private NotificationManager notificationManager;

    private UserAccessor userAccessor;

    private AutowatchManager autowatchManager;

    private static final Class<?>[] HANDLED_EVENTS = new Class<?>[]{CommentCreateEvent.class,
            PageCreateEvent.class, PageUpdateEvent.class, BlogPostCreateEvent.class, BlogPostUpdateEvent.class,
            AttachmentCreateEvent.class, AttachmentUpdateEvent.class};

    public void handleEvent( Event evt ) {
        String username;
        AbstractPage page;

        // Double-check we've gotten the right event.
        if ( evt instanceof CommentCreateEvent ) {
            CommentCreateEvent cce = ( CommentCreateEvent ) evt;

            if ( isAutowatching( cce.getComment().getSpaceKey(), AutowatchManager.COMMENTS_TYPE ) ) {
                username = cce.getComment().getLastModifierName();
                page = cce.getComment().getPage();
                watchPage( username, page );
            }
        } else if ( evt instanceof PageEvent ) {
            page = ( ( PageEvent ) evt ).getPage();
            username = page.getLastModifierName();

            if ( isAutowatching( page.getSpaceKey(), AutowatchManager.PAGES_TYPE ) ) {
                watchPage( username, page );
            }
        } else if ( evt instanceof BlogPostEvent ) {
            page = ( ( BlogPostEvent ) evt ).getBlogPost();
            username = page.getLastModifierName();

            if ( isAutowatching( page.getSpaceKey(), AutowatchManager.BLOG_POSTS_TYPE ) )
                watchPage( username, page );
        } else if ( evt instanceof AttachmentEvent ) {
            AttachmentEvent ae = ( AttachmentEvent ) evt;

            for ( Attachment a : ae.getAttachments() ) {
                if ( a.getContent() instanceof AbstractPage ) {
                    page = ( AbstractPage ) a.getContent();
                    username = a.getLastModifierName();

                    if ( isAutowatching( page.getSpaceKey(), AutowatchManager.ATTACHMENTS_TYPE ) ) {
                        watchPage( username, page );
                    }
                }
            }
        }

    }

    private void watchPage( String username, AbstractPage page ) {
        if ( username != null ) {
            User user = userAccessor.getUser( username );
            if ( !notificationManager.isUserWatchingPageOrSpace( user, page.getSpace(), page ) )
                notificationManager.addPageNotification( user, page );
        }
    }

    private boolean isAutowatching( String spaceKey, String type ) {
        Boolean spaceWatched = autowatchManager.getAutowatchState( spaceKey, type );
        Boolean defaultWatched = autowatchManager.getAutowatchState( null, type );

        if ( Boolean.TRUE.equals( defaultWatched ) ) {
            return !Boolean.FALSE.equals( spaceWatched );
        } else {
            return Boolean.TRUE.equals( spaceWatched );
        }
    }

    public Class<?>[] getHandledEventClasses() {
        return HANDLED_EVENTS;
    }
    
    public void setAutowatchManager( AutowatchManager autowatchManager ) {
        this.autowatchManager = autowatchManager;
    }

    public void setNotificationManager( NotificationManager notificationManager ) {
        this.notificationManager = notificationManager;
    }

    public void setUserAccessor( UserAccessor userAccessor ) {
        this.userAccessor = userAccessor;
    }
}
