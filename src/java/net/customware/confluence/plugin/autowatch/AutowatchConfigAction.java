/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.autowatch;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ListBuilder;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Configures Autowatch settings.
 */
public class AutowatchConfigAction extends ConfluenceActionSupport {
    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 6747917384931076873L;

    private SpaceManager spaceManager;

    private AutowatchManager autowatchManager;

    private String update;

    private boolean pages;

    private boolean blogposts;

    private boolean comments;

    private boolean attachments;

    public AutowatchConfigAction() {
    }

    @Override
    public String execute() throws Exception {

        if ( update != null ) {
            autowatchManager.setAutowatchState( null, AutowatchManager.PAGES_TYPE, Boolean.valueOf( pages ) );
            autowatchManager.setAutowatchState( null, AutowatchManager.BLOG_POSTS_TYPE, Boolean
                    .valueOf( blogposts ) );
            autowatchManager.setAutowatchState( null, AutowatchManager.COMMENTS_TYPE, Boolean.valueOf( comments ) );
            autowatchManager.setAutowatchState( null, AutowatchManager.ATTACHMENTS_TYPE, Boolean
                    .valueOf( attachments ) );

            HttpServletRequest req = ServletActionContext.getRequest();

            // TODO: Rewrite this to do actual paging.
            ListBuilder<Space> spaces = spaceManager.getSpaces( SpacesQuery.newQuery().forUser( getRemoteUser() )
                    .build() );
            for ( Space space : spaces.getRange( 0, spaces.getAvailableSize() ) ) {
                setAutowatchState( req, space, AutowatchManager.PAGES_TYPE );
                setAutowatchState( req, space, AutowatchManager.BLOG_POSTS_TYPE );
                setAutowatchState( req, space, AutowatchManager.COMMENTS_TYPE );
                setAutowatchState( req, space, AutowatchManager.ATTACHMENTS_TYPE );
            }
        }

        return SUCCESS;
    }

    private void setAutowatchState( HttpServletRequest req, Space space, String type ) {
        String watchVal = req.getParameter( space.getKey() + "." + type );
        Boolean watchBool = ( watchVal == null || watchVal.trim().length() == 0 ) ? null : ( "true"
                .equals( watchVal ) ? Boolean.TRUE : Boolean.FALSE );
        autowatchManager.setAutowatchState( space.getKey(), type, watchBool );

    }

    public void setSpaceManager( SpaceManager spaceManager ) {
        this.spaceManager = spaceManager;
    }

    public List<Space> getSpaces() {
        ListBuilder<Space> spaces = spaceManager.getSpaces( SpacesQuery.newQuery().forUser( getRemoteUser() )
                .build() );

        // TODO: Rewrite this to do actual paging.
        return spaces.getRange( 0, spaces.getAvailableSize() );
    }

    public void setAutowatchManager( AutowatchManager autowatchManager ) {
        this.autowatchManager = autowatchManager;
    }

    public AutowatchManager getAutowatchManager() {
        return autowatchManager;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate( String update ) {
        this.update = update;
    }

    public boolean isPages() {
        return pages;
    }

    public void setPages( boolean pages ) {
        this.pages = pages;
    }

    public boolean isBlogposts() {
        return blogposts;
    }

    public void setBlogposts( boolean blogposts ) {
        this.blogposts = blogposts;
    }

    public boolean isComments() {
        return comments;
    }

    public void setComments( boolean comments ) {
        this.comments = comments;
    }

    public boolean isAttachments() {
        return attachments;
    }

    public void setAttachments( boolean attachments ) {
        this.attachments = attachments;
    }

    @Override
    public boolean isPermitted() {
        return permissionManager.hasPermission( getRemoteUser(), Permission.ADMINISTER,
                PermissionManager.TARGET_APPLICATION );
    }
}
