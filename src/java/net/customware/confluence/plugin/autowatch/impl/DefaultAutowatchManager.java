/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.autowatch.impl;

import net.customware.confluence.plugin.autowatch.AutowatchManager;

import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;

/**
 * This class tracks the global settings for auto-watching.
 */
public class DefaultAutowatchManager implements AutowatchManager {
    private static final String LEGACY_AUTOWATCH_PREFIX = "org.randombits.confluence.autowatch.";

    private static final String AUTOWATCH_PREFIX = "net.customware.confluence.plugin.autowatch";

    private BandanaManager bandanaManager;

    private BandanaContext bandanaContext = ConfluenceBandanaContext.GLOBAL_CONTEXT;

    private Boolean watchByDefault;

    public DefaultAutowatchManager() {
    }

    public void setBandanaManager( BandanaManager bandanaManager ) {
        this.bandanaManager = bandanaManager;
    }

    private BandanaManager getBandanaManager() {
        return bandanaManager;
    }

    private Object getBandanaValue( String key ) {
        Object value = getBandanaManager().getValue( bandanaContext, AUTOWATCH_PREFIX + key );
        if ( value == null )
            value = getBandanaManager().getValue( bandanaContext, LEGACY_AUTOWATCH_PREFIX + key );

        return value;
    }

    private void setBandanaValue( String key, Object value ) {
        getBandanaManager().setValue( bandanaContext, AUTOWATCH_PREFIX + key, value );
    }

    private Boolean getWatchByDefault() {
        if ( watchByDefault == null ) {
            watchByDefault = ( Boolean ) getBandanaValue( "watchByDefault" );

            if ( watchByDefault == null )
                watchByDefault = Boolean.FALSE;
        }

        return watchByDefault;
    }

    private Boolean isOldSpaceAutowatched( String spaceKey ) {
        return ( Boolean ) getBandanaValue( "space." + spaceKey.toLowerCase() );
    }

    private void clearOldSpaceAutowatched( String spaceKey ) {
        setBandanaValue( "space." + spaceKey.toLowerCase(), null );
    }

    public void setAutowatchState( String spaceKey, String type, Boolean watched ) {
        setBandanaValue( getBandanaKey( spaceKey, type ), watched );
        if ( spaceKey != null )
            clearOldSpaceAutowatched( spaceKey );
    }

    public Boolean getAutowatchState( String spaceKey, String type ) {
        Boolean watchComments = ( Boolean ) getBandanaValue( getBandanaKey( spaceKey, type ) );
        if ( watchComments == null ) // Check the old, global default
        {
            if ( spaceKey == null )
                watchComments = getWatchByDefault();
            else
                watchComments = isOldSpaceAutowatched( spaceKey );
        }
        return watchComments;
    }

    private String getBandanaKey( String spaceKey, String property ) {
        StringBuffer buff = new StringBuffer();
        if ( spaceKey != null )
            buff.append( spaceKey ).append( "." );
        buff.append( property );

        return buff.toString();
    }
}
